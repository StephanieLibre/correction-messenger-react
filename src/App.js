import React, {useEffect, useState} from 'react';
import logo from './logo.svg';
import './App.css';
import axios from "axios"

function App() {
    const [user, setUser] = useState(null)
    useEffect(authenticate, []);

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return null;
    }

    function authenticate() {
        if (getCookie("authToken")) {
            axios.post("https://api.messenger.codecolliders.com/getUser", {
                authKey: getCookie("authToken")
            }).then(response => {
                setUser(response.data)
            })
        } else {
            axios.post("https://api.messenger.codecolliders.com/createUser", {})
                .then(response => {
                    console.log(response)
                    setCookie("authToken", response.data.authKey, 1)
                    setUser(response.data)
                })
        }
    }

    function displayUser() {
        if (user) {
            return <h1>{user.username}</h1>
        }
    }
    return (
        <div className="App">
            {displayUser()}
        </div>
    );
}

export default App;
